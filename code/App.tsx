import { Override, Data } from "framer"

const appState = Data({
    progress: 0,
    loading: false
})

export function ProgressBar(): Override {
    return {
        progress: appState.progress,
        onComplete: () => {
            appState.loading = false
        }
    }
}

export function Button(): Override {
    return {
        disabled: appState.loading,
        onTap: () => {
            if (appState.progress !== 100) {
                appState.progress = 100
                appState.loading = true
            }
            else {
                appState.progress = 0
                appState.loading = true
            }
            
        },
        text: (appState.progress === 100 && appState.loading === false) ? "Reset" : "Search"
    }
}
