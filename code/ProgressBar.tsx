import * as React from "react"
import { Frame, addPropertyControls, ControlType } from "framer"

export function ProgressBar(props) {
    const { progress, onComplete } = props

    function handleComplete() {
        onComplete()
    }

    return (
        <Frame
            width={300}
            height={10}
            background={"#ddd"}
            borderRadius={20}
            overflow="hidden"
        >
            <Frame
                background={"#4194F7"}
                height={"100%"}
                initial={{
                    width: "0%",
                }}
                animate={{
                    width: progress + "%",
                }}
                transition={{
                    ease: "linear",
                    duration: 2,
                }}
                onAnimationComplete={handleComplete}
            />
        </Frame>
    )
}

ProgressBar.defaultProps = {
    progress: 100,
    onComplete: () => null,
}

addPropertyControls(ProgressBar, {
    progress: {
        title: "Progress",
        type: ControlType.Number,
        min: 0,
        max: 100,
        step: 0.5,
        defaultValue: 0,
    },
})
